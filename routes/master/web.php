<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Master\ExamController;
use App\Http\Controllers\Master\QuestionController;
use App\Http\Controllers\Master\ExamSessionController;
use App\Http\Controllers\Master\QuestionPackageController;

Route::group(['prefix' => 'question-package'], function() {
    Route::get('/', [QuestionPackageController::class, 'index']);
    Route::post('/create', [QuestionPackageController::class, 'create']);
    Route::patch('/update/{id}', [QuestionPackageController::class, 'update']);
    Route::get('/dropdown-list', [QuestionPackageController::class, 'dropdownList']);
    Route::get('/{id}', [QuestionPackageController::class, 'show']);
    Route::delete('/destroy/{id}', [QuestionPackageController::class, 'destroy']);
    Route::delete('/force-destroy/{id}', [QuestionPackageController::class, 'forceDestroy']);
});

Route::group(['prefix' => 'question'], function() {
    Route::get('/', [QuestionController::class, 'index']);
    Route::post('/create', [QuestionController::class, 'create']);
    Route::post('/import', [QuestionController::class, 'import']);
    Route::get('/sample-import', [QuestionController::class, 'sampleImport']);
    Route::patch('/update/{id}', [QuestionController::class, 'update']);
    Route::post('/update-photo/{id}', [QuestionController::class, 'updatePhoto']);
    Route::get('/{id}', [QuestionController::class, 'show']);
    Route::delete('/destroy/{id}', [QuestionController::class, 'destroy']);
    Route::delete('/destroy-answer/{id}', [QuestionController::class, 'destroyAnswer']);
    Route::delete('/force-destroy/{id}', [QuestionController::class, 'forceDestroy']);
});

Route::group(['prefix' => 'exam'], function() {
    Route::get('/', [ExamController::class, 'index']);
    Route::post('/create', [ExamController::class, 'create']);
    Route::patch('/update/{id}', [ExamController::class, 'update']);
    Route::get('/{id}', [ExamController::class, 'show']);
    Route::delete('/destroy/{id}', [ExamController::class, 'destroy']);
    Route::delete('/force-destroy/{id}', [ExamController::class, 'forceDestroy']);
});

Route::group(['prefix' => 'exam-session'], function() {
    Route::get('/', [ExamSessionController::class, 'index']);
    Route::get('/list-student', [ExamSessionController::class, 'listStudent']);
    Route::post('/create', [ExamSessionController::class, 'create']);
    Route::post('/add-student/{id}', [ExamSessionController::class, 'addStudent']);
    Route::patch('/update/{id}', [ExamSessionController::class, 'update']);
    Route::get('/{id}', [ExamSessionController::class, 'show']);
    Route::delete('/destroy/{id}', [ExamSessionController::class, 'destroy']);
    Route::delete('/force-destroy/{id}', [ExamSessionController::class, 'forceDestroy']);
});
<?php

use App\Http\Controllers\Dashboard;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClassRoomController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LessonScheduleController;
use App\Http\Controllers\ClassRoomHistoryController;
use App\Http\Controllers\LessonScheduleMeetController;
use App\Http\Controllers\TeacherResponsibleController;
use App\Http\Controllers\LessonScheduleAttendanceController;
use App\Http\Controllers\Activity\LessonScheduleController as ActivityLessonScheduleController;
use App\Http\Controllers\Activity\History\LessonMeetHistoryController as ActivityLessonMeetHistoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'dashboard'], function() {
    Route::get('/count-lists', [DashboardController::class, 'listCounts']);
    Route::get('/schedule-lesson', [DashboardController::class, 'scheduleLesson']);
    Route::get('/teacher-responsible', [DashboardController::class, 'teacherResponsible']);
});

Route::group(['prefix' => 'class-room'], function() {
    Route::get('/', [ClassRoomController::class, 'index']);
    Route::get('/dropdown-list', [ClassRoomController::class, 'dropdownList']);
    Route::post('/create', [ClassRoomController::class, 'create']);
    Route::post('/import', [ClassRoomController::class, 'import']);
    Route::get('/export', [ClassRoomController::class, 'export']);
    Route::get('/sample-import', [ClassRoomController::class, 'sampleImport']);
    Route::get('/sample-import-student', [ClassRoomController::class, 'sampleImportStudent']);
    Route::patch('/update/{id}', [ClassRoomController::class, 'update']);
    Route::get('/list-students/{id}', [ClassRoomController::class, 'listStudents']);
    Route::get('/list-students', [ClassRoomController::class, 'listInputStudents']);
    Route::get('/clone/{id}', [ClassRoomController::class, 'clone']);
    Route::get('/{id}', [ClassRoomController::class, 'show']);
    Route::delete('/{id}', [ClassRoomController::class, 'destroy']);
});

Route::group(['prefix' => 'lesson-schedule'], function() {
    Route::get('/', [LessonScheduleController::class, 'index']);
    Route::get('/dropdown-list', [LessonScheduleController::class, 'dropdownList']);
    Route::post('/create', [LessonScheduleController::class, 'create']);
    Route::post('/import', [LessonScheduleController::class, 'import']);
    Route::get('/sample-import', [LessonScheduleController::class, 'sampleImport']);
    Route::patch('/update/{id}', [LessonScheduleController::class, 'update']);
    Route::get('/{id}', [LessonScheduleController::class, 'show']);
    Route::get('/list-students/{id}', [LessonScheduleController::class, 'listStudents']);
    Route::delete('/{id}', [LessonScheduleController::class, 'destroy']);
});

Route::group(['prefix' => 'lesson-schedule-meet'], function() {
    Route::get('/', [LessonScheduleMeetController::class, 'index']);
    Route::get('/dropdown-list', [LessonScheduleMeetController::class, 'dropdownList']);
    Route::post('/create', [LessonScheduleMeetController::class, 'create']);
    // Route::post('/import', [LessonScheduleMeetController::class, 'import']);
    // Route::get('/sample-import', [LessonScheduleMeetController::class, 'sampleImport']);
    Route::patch('/update/{id}', [LessonScheduleMeetController::class, 'update']);
    Route::get('/{id}', [LessonScheduleMeetController::class, 'show']);
    Route::delete('/{id}', [LessonScheduleMeetController::class, 'destroy']);
});

Route::group(['prefix' => 'lesson-schedule-attendance'], function() {
    Route::get('/', [LessonScheduleAttendanceController::class, 'index']);
    // Route::get('/dropdown-list', [LessonScheduleAttendanceController::class, 'dropdownList']);
    Route::post('/{lesson_schedule_meet_id}/attendance', [LessonScheduleAttendanceController::class, 'store']);
    // Route::post('/import', [LessonScheduleMeetController::class, 'import']);
    // Route::get('/sample-import', [LessonScheduleMeetController::class, 'sampleImport']);
    Route::get('/{lesson_schedule_meet_id}', [LessonScheduleAttendanceController::class, 'show']);
    Route::delete('/{lesson_schedule_meet_id}', [LessonScheduleAttendanceController::class, 'destroy']);
});

Route::group(['prefix' => 'teacher-responsible'], function() {
    Route::get('/', [TeacherResponsibleController::class, 'index']);
    Route::get('/{id}', [TeacherResponsibleController::class, 'show']);
});

Route::group(['prefix' => 'activity'], function() {
    Route::group(['prefix' => 'lesson-schedule'], function() {
        Route::get('/', [ActivityLessonScheduleController::class, 'index']);
        Route::get('/lesson-meet-history', [ActivityLessonMeetHistoryController::class, 'lessonMeetHistory']);
        Route::get('/lesson-meet-history/{meetId}', [ActivityLessonMeetHistoryController::class, 'showLessonMeetHistory']);
        Route::get('/{id}', [ActivityLessonScheduleController::class, 'show']);
        Route::post('/{id}/confirm-attendance', [ActivityLessonScheduleController::class, 'confirmAttendance']);
        Route::post('/{id}/confirm-meet-close', [ActivityLessonScheduleController::class, 'confirmMeetClose']);
        Route::get('/{id}/student-list', [ActivityLessonScheduleController::class, 'studentList']);
        Route::get('/{id}/list-meets', [ActivityLessonScheduleController::class, 'listMeets']);
        Route::get('/{id}/student-absent-list/{studentId}', [ActivityLessonScheduleController::class, 'studentAbsentList']);
        Route::get('/{meetId}/list-input-attendance-students', [ActivityLessonScheduleController::class, 'listInputAttendanceStudents']);
        Route::post('/{id}/student-absent', [ActivityLessonScheduleController::class, 'studentAbsent']);
        Route::post('/{id}/submit-agenda', [ActivityLessonScheduleController::class, 'submitAgenda']);
        
    });
});

Route::group(['prefix' => 'class-room-history'], function() {
    Route::get('/teacher-responsible/{teacherResponsibleId}', [ClassRoomHistoryController::class, 'showHistoryClassRoom']);
    Route::get('/teacher-responsible-class-room/{classRoomId}', [ClassRoomHistoryController::class, 'showHistoryTeacherResponsibleOnClassRoom']);
});

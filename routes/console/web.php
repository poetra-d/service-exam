<?php

use App\Http\Controllers\Console\OrganizationLevelController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'organization-level'], function() {
    Route::get('list', [OrganizationLevelController::class, 'index']);
});

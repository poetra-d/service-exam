<?php

namespace App\Imports\Import;

use Exception;
use App\Models\Question;
use App\Models\QuestionAnswer;
use App\Models\QuestionPackage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class QuestionImport implements ToCollection, WithHeadingRow
{
    protected $_organizationId;
    protected $_questionPackageId;

    public $data;
    public $dataDetail;
    public $error = false;
    public $errorMessage = '';
    public function __construct($organizationId, $questionPackageId)
    {
        $this->_organizationId = $organizationId;
        $this->_questionPackageId = $questionPackageId;
    }

    public function collection(Collection $collection)
    {
        DB::beginTransaction();
        try {
            foreach ($collection as $key => $row) {
                $rowNumber = $key+1;
                if (
                    !isset($row['soal']) ||
                    !isset($row['pilihan_a']) ||
                    !isset($row['pilihan_b']) ||
                    !isset($row['pilihan_c']) ||
                    // !isset($row['pilihan_d']) ||
                    // !isset($row['pilihan_e']) ||
                    !isset($row['kunci_jawaban']) ||
                    !isset($row['waktu']) ||
                    !isset($row['score'])
                ) {
                    $this->error = true;
                    $this->errorMessage = 'Ada kesalahan dalam dokumen, silakan sesuaikan dengan format dan import kembali';
                    return;
                }

                $questionPackage = QuestionPackage::org($this->_organizationId)->where('id', $this->_questionPackageId)->first();
                if (!$questionPackage) {
                    throw new Exception("Ada kesalahan dalam dokumen paket soal, tidak ditemukan");
                    return;
                }

                $answerCount = 1;
                foreach ($row as $columnName => $type) {
                    $answerKey = $row['kunci_jawaban'];
                    if ($columnName !== 'soal'
                        && $columnName !== 'kunci_jawaban'
                        && $columnName !== 'waktu'
                        && $columnName !== 'score') {

                            $questionData = [
                                'organization_id' => $this->_organizationId,
                                'question_package_id' => $this->_questionPackageId,
                                'course_id' => $questionPackage->course_id,
                                'question' => $row['soal'],
                                'time' => $row['waktu'],
                                'score' => $row['score']
                            ];
                            $this->data[] = $questionData;
                            $question = Question::firstOrNew($questionData);
                            $question->save();

                            $questionAnswerData = [
                                'organization_id' => $this->_organizationId,
                                'question_id' => $question->question_id,
                                'answer' => $type,
                            ];
                            $this->dataDetail[] = $questionAnswerData;
                            $questionAnswer = QuestionAnswer::firstOrNew($questionAnswerData);
                            if ($answerKey == $answerCount) {
                                $questionAnswer->is_correct = QuestionAnswer::IS_CORRECT;
                            }
                            $questionAnswer->save();
                            $answerCount++;
                    }
                }
            }
            
            DB::commit();
        } catch(Exception $ex) {
            DB::rollBack();
            $this->error = true;
            $this->errorMessage = $ex->getMessage();
        }
        
    }
}


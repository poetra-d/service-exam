<?php

namespace App\Http\Controllers\Master;

use Exception;
use App\Models\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\ExamDetail;
use App\Models\QuestionPackage;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    public function index(Request $request) {
        $paginate = $request->get('perpage', 20);
        $models = Exam::org($this->getOrgId())->orderBy('name');

        $models = $models->paginate($paginate);
        return $this->responseJson($models);
    }

    public function show($id) {
        $model = Exam::org($this->getOrgId())
            ->with([
                'examDetails',
                'questionPackages'
            ])
            ->find($id);
        if (!$model) return $this->responseNotFound();

        return $this->responseJson($model);
    }

    public function create(Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'duration' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = new Exam();
            $model->organization_id = $orgId;
            $model->fill($request->all());
            $model->save();

            foreach ($request->exam_details as $ed) {
                $examDetail = new ExamDetail();
                $examDetail->organization_id = $orgId;
                $examDetail->course_id = $model->course_id;
                $examDetail->exam_id = $model->id;
                $examDetail->question_package_id = $ed['question_package_id'];
                $examDetail->save();
            }

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseJson($model, 200, 'Berhasil');
    }

    public function update($id, Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'duration' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = Exam::org($orgId)->find($id);
            if (!$model) return $this->responseNotFound();
            $model->fill($request->all());
            $model->save();

            $oldEd = [];
            $getOldEd = ExamDetail::query()
                ->where('exam_id', $model->id)
                ->pluck('id');
            $oldEd = collect($getOldEd);

            $edIds = [];
            foreach ($request->exam_details as $ed) {
                $modelEd = ExamDetail::query()
                    ->where('question_package_id', $ed['question_package_id'])
                    ->where('exam_id', $model->id)
                    ->first();

                if (!$modelEd) {
                    $modelQp = QuestionPackage::query()
                        ->findOrFail($ed['question_package_id']);
    
                    $modelEd = new ExamDetail();
                    $modelEd->organization_id = $orgId;
                    $modelEd->course_id = $model->course_id;
                    $modelEd->exam_id = $model->id;
                    $modelEd->fill($modelQp->only(['question_package_id']));
                }

                $modelEd->organization_id = $orgId;
                $modelEd->course_id = $model->course_id;
                $modelEd->exam_id = $model->id;
                $modelEd->fill($ed);
                $modelEd->save();

                $edIds[] = $modelEd->id;
            }

            $missingIds = $oldEd->diff($edIds)->values();
            ExamDetail::destroy($missingIds);

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }


        return $this->responseJson($model, 200, 'Update Berhasil');
    }

    public function destroy(string $id)
    {
        $model = Exam::org($this->getOrgId())->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->examDetails()->delete();
            $model->delete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus');
    }

    public function forceDestroy($id)
    {
        $model = Exam::org($this->getOrgId())
            ->withTrashed()
            ->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->examDetails()->forceDelete();
            $model->forceDelete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus secara permanen');
    }
}

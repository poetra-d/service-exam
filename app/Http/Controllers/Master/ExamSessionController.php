<?php

namespace App\Http\Controllers\Master;

use Exception;
use App\Models\ExamSession;
use Illuminate\Http\Request;
use App\Models\ExamSessionDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentCandidate;
use Illuminate\Support\Facades\Validator;

class ExamSessionController extends Controller
{
    public function index(Request $request) {
        $paginate = $request->get('perpage', 20);
        $models = ExamSession::org($this->getOrgId())->orderBy('name');

        $models = $models->paginate($paginate);
        return $this->responseJson($models);
    }

    public function show($id) {
        $model = ExamSession::org($this->getOrgId())
            ->with([
                'exam',
            ])
            ->find($id);
        if (!$model) return $this->responseNotFound();

        return $this->responseJson($model);
    }

    public function create(Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = new ExamSession();
            $model->organization_id = $orgId;
            $model->fill($request->all());
            $model->save();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseJson($model, 200, 'Berhasil');
    }

    public function update($id, Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'duration' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = ExamSession::org($orgId)->find($id);
            if (!$model) return $this->responseNotFound();
            $model->fill($request->all());
            $model->save();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }


        return $this->responseJson($model, 200, 'Update Berhasil');
    }

    public function destroy(string $id)
    {
        $model = ExamSession::org($this->getOrgId())->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->examDetails()->delete();
            $model->delete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus');
    }

    public function forceDestroy($id)
    {
        $model = ExamSession::org($this->getOrgId())
            ->withTrashed()
            ->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->examDetails()->forceDelete();
            $model->forceDelete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus secara permanen');
    }

    public function listStudent(Request $request) {
        $orgId = $this->getOrgId();
        $paginate = $request->get('perpage', 20);
        $search = strtolower($request->get('search', null));
        $type = $request->get('type', null);

        if ($type == ExamSession::TYPE_STUDENT_CANDIDATE) {
            $models = StudentCandidate::org($orgId)->orderBy('name');

            if ($search) {
                $models = $models->where(DB::raw('LOWER(name)'), 'like', "%$search%")
                    ->orWhere('nisn', "$search");
            }
        } else {
            // $models = Student::org($orgId)->orderBy('name');

            // if ($search) {
            //     $models = $models->where(DB::raw('LOWER(name)'), 'like', "%$search%")
            //         ->orWhere('nis', "$search")
            //         ->orWhere('nisn', "$search");
            // }
            // todo list student regular
            return $this->responseJson([], 200);
        }

        $models = $models->paginate($paginate); 

        return $this->responseJson($models, 200);
    }

    public function addStudent($id, Request $request) {
        $orgId = $this->getOrgId();
        // $validator = Validator::make($request->all(), [
        //     'title' => 'required',
        // ]);
        // if ($validator->fails()) {
        //     return $this->responseValidator($validator);
        // }

        DB::beginTransaction();
        try {
            $model = ExamSession::org($orgId)->find($id);
            if (!$model) return $this->responseNotFound();

            foreach ($request->student_list as $sl) {
                $modelEsd = new ExamSessionDetail();
                $modelEsd->exam_session_id = $model->id;
                $modelEsd->exam_id = $model->exam_id;
                $modelEsd->type = $model->type;
                $modelEsd->organization_id = $orgId;
                $modelEsd->student_candidate_id = $sl['student_candidate_id'];
                $modelEsd->save();
            }

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseJson($model, 200, 'Berhasil');
    }
}

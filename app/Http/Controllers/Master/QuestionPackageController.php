<?php

namespace App\Http\Controllers\Master;

use Exception;
use Illuminate\Http\Request;
use App\Models\QuestionPackage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class QuestionPackageController extends Controller
{
    public function index(Request $request) {
        $paginate = $request->get('perpage', 20);
        $search = strtolower($request->get('search', null));
        $models = QuestionPackage::org($this->getOrgId())
        ->with([
            'course'
        ])
        ->orderBy('name');

        if ($search) {
            $models = $models->where(DB::raw('LOWER(name)'), 'like', "%$search%");
        }

        $models = $models->paginate($paginate);
        return $this->responseJson($models);
    }

    public function show($id) {
        $model = QuestionPackage::org($this->getOrgId())
            ->with([
                'course',
                'questions'
            ])
            ->find($id);
        if (!$model) return $this->responseNotFound();

        return $this->responseJson($model);
    }

    public function create(Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'code' => [
                'required',
                Rule::unique('exam.question_package', 'code')->where('organization_id', $orgId)->withoutTrashed()
            ],
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = new QuestionPackage();
            $model->organization_id = $orgId;
            $model->fill($request->all());
            $model->save();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseJson($model, 200, 'Berhasil');
    }

    public function update($id, Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'code' => [
                'required',
                Rule::unique('exam.question_package', 'code')->where('organization_id', $orgId)->whereNot('id', $id)->withoutTrashed()
            ],
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = QuestionPackage::org($orgId)->find($id);
            if (!$model) return $this->responseNotFound();
            $model->fill($request->all());
            $model->save();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }


        return $this->responseJson($model, 200, 'Update Berhasil');
    }

    public function destroy(string $id)
    {
        $model = QuestionPackage::org($this->getOrgId())->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->delete();
            
            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus');
    }

    public function forceDestroy($id)
    {
        $model = QuestionPackage::org($this->getOrgId())
            ->withTrashed()
            ->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->forceDelete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus secara permanen');
    }

    public function dropdownList(Request $request) {
        $search = strtolower($request->get('search', null));

        $models = QuestionPackage::org($this->getOrgId())->orderBy('name');
        if ($search) {
            $models = $models->where(DB::raw('LOWER(name)'), 'like', "%$search%")
                ->orWhere('code', 'like', "%$search%");
        }

        $models = $models->select(['id', DB::raw("CONCAT(code, ' - ', name) AS name")])->get();

        return $this->responseJson($models);
    }
}

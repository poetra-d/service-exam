<?php

namespace App\Http\Controllers\Master;

use Exception;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Models\QuestionAnswer;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\Import\QuestionImport;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    public function index(Request $request) {
        $paginate = $request->get('perpage', 20);
        $search = strtolower($request->get('search', null));
        $models = Question::org($this->getOrgId())->orderBy('created_at');
        $questionPackageId = $request->get('question_package_id', null);

        if ($questionPackageId) {
            $models = $models->where('question_package_id', $questionPackageId);
        }
        if ($search) {
            $models = $models->where(DB::raw('LOWER(question)'), 'like', "%$search%");
        }

        $models = $models->paginate($paginate);
        return $this->responseJson($models);
    }

    public function show($id) {
        $model = Question::org($this->getOrgId())
        ->with([
            'course',
            'questionAnswers',
            'questionPackage'
        ])
        ->find($id);
        if (!$model) return $this->responseNotFound();

        return $this->responseJson($model);
    }

    public function create(Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'course_id' => 'required|numeric',
            'question_package_id' => 'required|numeric',
            'questions' => 'required|array',
            'questions.*.time' => 'required|numeric',
            'questions.*.score' => 'required|numeric',
            'questions.*.question' => 'required|string',
            'questions.*.answers' => 'required|array',
            'questions.*.answers.*.answer' => 'required|string',
            'questions.*.answers.*.is_correct' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            foreach ($request->questions as $questionData) {
                $question = Question::create([
                    'organization_id' => $orgId,
                    'course_id' => $request->course_id,
                    'question_package_id' => $request->question_package_id,
                    'time' => $questionData['time'],
                    'score' => $questionData['score'],
                    'question' => $questionData['question'],
                ]);

                foreach ($questionData['answers'] as $answerData) {
                    QuestionAnswer::create([
                        'organization_id' => $orgId,
                        'question_id' => $question->id,
                        'answer' => $answerData['answer'],
                        'is_correct' => $answerData['is_correct'],
                    ]);
                }
            }
            
            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseJson($question, 200, 'Berhasil');
    }

    public function update($id, Request $request) {
        $orgId = $this->getOrgId();
        $validator = Validator::make($request->all(), [
            'course_id' => 'required|numeric',
            'question_package_id' => 'required|numeric',
            // 'questions' => 'required|array',
            'questions.*.time' => 'required|numeric',
            'questions.*.score' => 'required|numeric',
            'questions.*.question' => 'required|string',
            'questions.*.answers' => 'required|array',
            'questions.*.answers.*.answer' => 'required|string',
            'questions.*.answers.*.is_correct' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        DB::beginTransaction();
        try {
            $model = Question::org($orgId)->find($id);
            if (!$model) return $this->responseNotFound();
            $model->fill($request->all());
            $model->save();

            foreach ($request->answers as $answer) {
                QuestionAnswer::updateOrCreate(
                    [ 
                        'id' => $answer['id'] 
                    ],
                    [
                        'organization_id' => $orgId,
                        'question_id' => $model->id,
                        'answer' => $answer['answer'],
                        'is_correct' => $answer['is_correct'],
                    ]
                );
            }

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseJson($model, 200, 'Update Berhasil');
    }

    public function destroy(string $id)
    {
        $model = Question::org($this->getOrgId())->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->questionAnswers()->delete();
            $model->delete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus');
    }

    public function forceDestroy($id)
    {
        $model = Question::org($this->getOrgId())
            ->withTrashed()
            ->find($id);
        if (!$model) return $this->responseNotFound();

        DB::beginTransaction();
        try {
            $model->questionAnswers()->forceDelete();
            $model->forceDelete();

            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil dihapus secara permanen');
    }

    public function destroyAnswer(string $id)
    {
        $model = QuestionAnswer::org($this->getOrgId())
            ->withTrashed()
            ->find($id);
        if (!$model) return $this->responseNotFound();

        $model->delete();

        return $this->responseSuccess('Jawaban berhasil dihapus');
    }

    public function import(Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => ['required'],
        ]);
        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        $questionPackageId = $request->get('question_package_id');
        $file = $request->file('file');
        if (!in_array($file->getClientOriginalExtension(), ['xlsx', 'xls'])) {
            return $this->responseError('File harus berekstensi .xlsx atau .xls');
        }
        
        DB::beginTransaction();
        try {
            $import = new QuestionImport($this->getOrgId(), $questionPackageId);
            Excel::import($import, $file);
            if ($import->error) {
                throw new Exception($import->errorMessage);
            }
            DB::commit();
        } catch (Exception $err) {
            DB::rollBack();
            return $this->responseError($err->getMessage());
        }

        return $this->responseSuccess('Berhasil');
    }

    public function sampleImport() {
        return $this->responseJson(url('template-imports/soal-dan-jawaban.xlsx'));
    }

    public function updatePhoto($id, Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'image | mimes:jpeg,png,jpg | max:2000',
        ]);

        if ($validator->fails()) {
            return $this->responseValidator($validator);
        }

        $subdomain = $request->header('subdomain');
        $model = Question::org($this->getOrgId())->find($id);
        if (!$model) return $this->responseNotFound();

        if (!$request->has('file')) {
            return $this->responseError('File tidak ditemukan, harap upload kembali');
        }
        $code = $model->questionPackage->code;

        $photo = $request->file('file');
        $model->deletePhoto();
        $filename = "{$subdomain}"."-{$code}-photo-".time().".{$photo->extension()}";
        $photo->storeAs($model->getPhotoPath(), $filename);

        $data = [
            'organization_id' => $model->organization_id,
            'photo' => $filename,
        ];
        $model->update($data);
        return $this->responseJson(null, 200, 'Photo berhasil diubah');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as Middleware;

class ApiKey extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        $apiKeys = [
            '0peT3YLAyygKoc2GS8dgstH7OdPw'
        ];
        $apiKey = $request->header('x-api-key', null);
        if (!$apiKey) {
            return response()->json([
                'status' => 403,
                'message' => 'Invalid api key',
            ], 403);
        }
        if (!in_array($apiKey, $apiKeys)) {
            return response()->json([
                'status' => 403,
                'message' => 'Invalid api key.',
            ], 403);
        }

        return $next($request);
    }
}

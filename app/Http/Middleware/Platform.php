<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as Middleware;

class Platform extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        $platform = $request->header('platform', null);
        if (!$platform) {
            return response()->json([
                'status' => 404,
                'message' => 'Invalid platform'
            ], 404);
        }
        $platforms = ['dashboard','client'];
        if (!in_array($platform, $platforms)) {
            return response()->json([
                'status' => 404,
                'message' => 'Platform not registered'
            ], 404);
        }
        return $next($request);
    }
}

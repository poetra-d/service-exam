<?php

namespace App\Http\Middleware;

use App\Models\Organization;
use Closure;
use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as Middleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class Authorize extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if (!$user) {
                return response()->json([
                    'status' => 401,
                    'message' => __('Unauthorize')
                ], 401);
            }
            $subdomain = $request->header('subdomain', null);
            $payload = JWTAuth::parseToken()->getPayload();
            if ($subdomain != base64_decode($payload['dom'])) {
                return response()->json([
                    'status' => 404,
                    'message' => __('Token with subdomain do not match')
                ], 404);
            }

            return $next($request);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'status' => 401,
                'message' => 'Token Invalid'
            ], 401);
        }
    }
}

<?php

namespace App\Http\Middleware;

use App\Models\Organization;
use Closure;
use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as Middleware;

class Subdomain extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        $subdomain = $request->header('subdomain', null);
        if (!$subdomain) {
            return response()->json([
                'status' => 404,
                'message' => 'You cannot access without subdomain'
            ], 404);
        }
        $organization = Organization::where('code', $subdomain)->first();
        if (!$organization) {
            return response()->json([
                'status' => 404,
                'message' => 'Organization not registered'
            ], 404);
        }
        return $next($request);
    }
}

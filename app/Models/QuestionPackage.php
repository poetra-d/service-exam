<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionPackage extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.question_package';

    protected $fillable = [
        'organization_id',
        'course_id',
        'code',
        'name',
        'description',
        'actived'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function course() {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'question_package_id', 'id');
    }
}

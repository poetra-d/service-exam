<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdministratorRole extends BaseModel
{
    use HasFactory, Blameable;

    const ROLE_TEACHER = 1;
    const ROLE_STAFF = 2;
    const ROLE_HEADMASTER = 3;

    protected $table = 'org.administrator_role';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'administrator_id',
        'm_role_id',
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function mRole() {
        return $this->hasOne(MRole::class, 'id', 'm_role_id');
    }

    public function administrator() {
        return $this->hasOne(Administrator::class, 'id', 'administrator_id');
    }
}

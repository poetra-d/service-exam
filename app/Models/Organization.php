<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Organization extends BaseModel
{
    use HasFactory;

    protected $table = 'org.organization';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'actived',
    ];

    public function data() {
        return $this->hasOne(OrganizationData::class, 'organization_id', 'id');
    }
}

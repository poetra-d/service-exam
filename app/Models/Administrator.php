<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Administrator extends BaseModel
{
    use HasFactory;

    protected $table = 'org.administrator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'nip',
        'name',
        'email',
        'actived',
        'is_teacher',
        'employment_status'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'organization_id', 'id');
    }

    public function data() {
        return $this->hasOne(AdministratorData::class, 'administrator_id', 'id');
    }

    public function userClient() {
        return $this->hasOne(UserClient::class, 'external_id', 'id')
            ->where('external_table', UserClient::EXTERNAL_ADMINISTRATOR);
    }

    public function administratorRoles() {
        return $this->hasOne(AdministratorRole::class, 'administrator_id', 'id');
    }

    public function roles() {
        return $this->belongsToMany(MRole::class, 'org.administrator_role', 'administrator_id', 'm_role_id');
    }

    public function classResponsibles() {
        return $this->hasMany(ClassRoom::class, 'teacher_responsible_id', 'id');
    }

    public function classRoomHistories()
    {
        return $this->hasMany(ClassRoomHistory::class, 'teacher_id', 'id');
    }
}

<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StudentCandidate extends BaseModel
{
    use HasFactory, Blameable, SoftDeletes;

    protected $table = 'org.student_candidate';

    protected $fillable = [
        'organization_id',
        'nisn',
        'name',
        'pob',
        'dob',
        'gender',
        'religion',
        'address',
        'province_id',
        'city_id',
        'district_id',
        'sub_district_id',
        'actived',
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
    
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id', 'id');
    }

    public function userClient() {
        return $this->hasOne(UserClient::class, 'external_id', 'id')
            ->where('external_table', UserClient::EXTERNAL_STUDENT_CANDIDATE);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends BaseModel
{
    use HasFactory;

    protected $table = 'org.student';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'nisn',
        'nis',
        'name',
        'actived',
        'graduated',
        'major_id',
        'class_id',
        'class_room_id',
        'current_academic_year_id',
        'current_semester_id',
        'email'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function data() {
        return $this->hasOne(StudentData::class, 'student_id', 'id');
    }

    public function userClient() {
        return $this->hasOne(UserClient::class, 'external_id', 'id')
            ->where('external_table', UserClient::EXTERNAL_STUDENT);
    }

    public function classRoomStudents() {
        return $this->hasMany(ClassRoomStudent::class, 'student_id', 'id');
    }

    public function classRoom() {
        return $this->hasOne(ClassRoom::class, 'id', 'class_room_id');
    }
}

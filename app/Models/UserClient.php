<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class UserClient extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const EXTERNAL_STUDENT = 'student';
    const EXTERNAL_STUDENT_PARENT = 'student_parent';
    const EXTERNAL_ADMINISTRATOR = 'administrator';
    const EXTERNAL_STUDENT_CANDIDATE = 'student_candidate';

    protected $table = 'org.user_client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'external_table',
        'student_id',
        'email',
        'student_parent_id',
        'username',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function getUserPermissionsAttribute()
    {
        return $this->getUserPermissions();
    }

    public function getUserMenusAttribute()
    {
        return $this->getUserMenus();
    }

    public function getUserPermissions()
    {
        $admRole = AdministratorRole::where('administrator_id', $this->id)
        ->whereHas('mRole', function ($query) {
            $query->where('type', MRole::TYPE_PERMISSION);
        });

        $admRole = $admRole->pluck('m_role_id')->toArray();

        $collection = collect($this->admin_roles);
        $results = $collection->flatten()->map(function ($item) {
            return [$item->id];
        });

        $mRoleAccess = MRoleAccess::whereIn('m_role_id', $results)
            ->pluck('m_role_permission_id')
            ->toArray();

        $mRolePermissions = MRole::whereIn('id', $mRoleAccess)
            ->pluck('code')
            ->toArray();

        return array_merge($admRole, $mRolePermissions);
    }

    public function getUserMenus()
    {
        $modelMenu = new MMenu();

        if (in_array('/*', $this->user_permissions)) {
            $assigned = $modelMenu->pluck('id')->toArray();
        } else {
            $assigned = $modelMenu->whereIn('url', $this->user_permissions)
                ->pluck('id')->toArray();
        }

        $menus = $modelMenu->get()->keyBy('id')->toArray();

        $result = self::normalizeMenu($assigned, $menus, null, null);

        return $result;
    }

    public function can($permission, $arguments = [])
    {
        if (in_array($permission, ['/site', '/auth/auto', '/site/layout'])) {
            return true;
        }
        $admRole = AdministratorRole::where('administrator_id', $this->id);

        $admRole = $admRole->pluck('m_role_id')->toArray();

        $collection = collect($this->admin_roles);
        $results = $collection->flatten()->map(function ($item) {
            return [$item->id];
        });

        $mRole = MRole::whereIn('id', $results)
            ->pluck('code')
            ->toArray();
        
        if (count($mRole) <= 0) {
            return false;
        }

        // special permission
        if (in_array('/*', $mRole)) {
            return true;
        }

        // return true if permission equals item name
        if (in_array($permission, $mRole)) {
            return true;
        }

        $mRoleAccess = MRoleAccess::whereIn('m_role_id', $admRole)
            ->pluck('m_role_permission_id')
            ->toArray();

        $mRoleAccessMap = MRole::whereIn('id', $mRoleAccess)
            ->pluck('code')
            ->toArray();

        // special permission
        if (in_array('/*', $mRoleAccessMap)) {
            return true;
        }

        // return true if permission equals item name
        if (in_array($permission, $mRoleAccessMap)) {
            return true;
        }

        return false;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'org' => $this->organization_id,
            'dom' => base64_encode($this->organization->code ?? ''),
        ];
    }

    public function student() {
        if ($this->external_table == self::EXTERNAL_STUDENT) {
            return $this->hasOne(Student::class, 'id', 'external_id');
        } else {
            return null;
        }
    }

    public function studentParent() {
        if ($this->external_table == self::EXTERNAL_STUDENT_PARENT) {
            return $this->hasOne(StudentParent::class, 'id', 'external_id');
        } else {
            return null;
        }
    }

    public function administrator() {
        if ($this->external_table == self::EXTERNAL_ADMINISTRATOR) {
            return $this->hasOne(Administrator::class, 'id', 'external_id');
        } else {
            return null;
        }
    }

    public function administratorAgenda() {
        if ($this->external_table == self::EXTERNAL_ADMINISTRATOR) {
            return $this->hasOne(AdministratorAgenda::class, 'id', 'external_id');
        } else {
            return null;
        }
    }

    public function studentCandidate() {
        if ($this->external_table == self::EXTERNAL_STUDENT_CANDIDATE) {
            return $this->hasOne(StudentCandidate::class, 'id', 'external_id');
        } else {
            return null;
        }
    }

    public function administratorRoles() {
        return $this->administrator() ? $this->administrator->roles : [];
    }

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function getAdditionalAttributes() {
        switch ($this->external_table) {
            case self::EXTERNAL_STUDENT :
                $student = $this->student ?? new Student();
                return [
                    'name' => $student->name,
                    'code' => $student->nis,
                ];
            case self::EXTERNAL_STUDENT_PARENT :
                $parent = $this->studentParent ?? new StudentParent();
                return [
                    'name' => $parent->name,
                    'code' => '',
                ];
            case self::EXTERNAL_ADMINISTRATOR :
                $admin = $this->administrator ?? new Administrator();
                return [
                    'name' => $admin->name,
                    'code' => $admin->nip,
                ];
            case self::EXTERNAL_STUDENT_CANDIDATE :
                $admin = $this->studentCandidate ?? new StudentCandidate();
                return [
                    'name' => $admin->name,
                    'code' => $admin->nisn,
                ];
            default:
                return [
                    'name' => null,
                    'code' => null,
                ];
        }
    }

    private static function normalizeMenu(&$assigned, &$menus, $callback, $parent = null, $root = null)
    {
        $result = [];
        $order = [];
        foreach ($assigned as $id) {
            $menu = $menus[$id];
            $root = $root == null ? $parent : $menus[$root]['id'];
            if ($menu['parent_id'] == $parent) {
                $menu['children'] = static::normalizeMenu($assigned, $menus, $callback, $id, $root);
                if ($callback !== null) {
                    $item = call_user_func($callback, $menu);
                } else {
                    $item = [
                        'parent_id' => $root,
                        'id' => $menu['id'], 
                        'name' => $menu['name'],
                        'url' => $menu['url'],
                        'order' => $menu['order'],
                        'link_type' => $menu['link_type'],
                        'icon' => $menu['icon'],
                    ];
                    if ($menu['children'] != []) {
                        $item['items'] = $menu['children'];
                    }
                }
                $result[] = $item;
                $order[] = $menu['order']; 
            }
        }
        if ($result != []) {
            array_multisort($order, $result);
        }

        return $result;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class MRoleAccess extends BaseModel
{
    use HasFactory;

    protected $table = 'org.m_role_access';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'm_role_id',
        'm_role_permission_id'
    ];

    public function role()
    {
        return $this->belongsTo(MRole::class, 'm_role_id');
    }

    public function permission()
    {
        return $this->belongsTo(MRole::class, 'm_role_permission_id');
    }
}

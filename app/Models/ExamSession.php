<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamSession extends BaseModel
{
    const TYPE_REGULAR = 1;
    const TYPE_STUDENT_CANDIDATE = 2;

    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.exam_session';

    protected $fillable = [
        'organization_id',
        'exam_id',
        'title',
        'start_time',
        'end_time',
        'type'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function exam() {
        return $this->hasOne(Exam::class, 'id', 'exam_id');
    }
}

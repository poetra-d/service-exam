<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrganizationLevel extends BaseModel
{
    use HasFactory;

    protected $table = 'org.organization_level';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'actived',
    ];
}

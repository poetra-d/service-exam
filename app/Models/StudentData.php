<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StudentData extends BaseModel
{
    use HasFactory;

    protected $table = 'org.student_data';
    protected $appends = [
        'photo_url'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'student_id',
        'photo',
        'phone',
        'pob',
        'dob',
        'gender',
        'religion',
        'address',
        'village_name',
        'district_name',
        'city_name',
        'province_name',
        'postal_code',
        'father_name',
        'father_work',
        'mother_name',
        'mother_work',
        'parent_address',
        'parent_village_name',
        'parent_district_name',
        'parent_city_name',
        'parent_province_name',
        'parent_postal_code',
        'status_in_family', 'order_in_family', 'home_phone', 'origin_school', 'graduation_year', 'guardian_name', 'guardian_phone', 'guardian_address',
        'guardian_work', 'father_phone', 'father_email', 'father_pob', 'father_dob', 'father_last_education', 'father_position_at_work', 'father_work_address', 'father_monthly_income',
        'mother_phone', 'mother_email', 'mother_pob', 'mother_dob', 'mother_last_education', 'mother_position_at_work', 'mother_work_address', 'mother_monthly_income',
        'about_me',
        'province_id',
        'city_id',
        'district_id',
        'sub_district_id'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }
    
    public function getPhotoPath() {
        $code = $this->organization->code;
        return ("public/organization/{$code}/student");
    }

    public function getPhotoUrlAttribute() {
        $code = $this->organization->code;
        $photo = $this->photo;
        if ($photo == null) return null;
        return url(Storage::url("organization/$code/student/$photo"));
    }

    public function deletePhoto(): bool {
        if ($this->photo) {
            Storage::delete($this->getPhotoPath().'/'.$this->photo);
            return true;
        }
        return false;
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
    
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id', 'id');
    }
}

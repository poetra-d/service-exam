<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamDetail extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.exam_detail';

    protected $fillable = [
        'organization_id',
        'course_id',
        'question_package_id',
        'description',
        'actived'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function course() {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function questionPackage() {
        return $this->hasOne(QuestionPackage::class, 'id', 'question_package_id');
    }
}

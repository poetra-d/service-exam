<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Question extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.question';

    protected $fillable = [
        'organization_id',
        'question_package_id',
        'course_id',
        'photo',
        'time',
        'score',
        'question',
        'actived'
    ];

    protected $appends = [
        'photo_url',
    ];

    public function getPhotoPath() {
        $code = $this->organization->code;
        return ("public/organization/{$code}/question");
    }

    public function getPhotoUrlAttribute() {
        $code = $this->organization->code;
        $photo = $this->photo;
        if ($photo == null) return null;
        return url(Storage::url("organization/$code/question/$photo"));
    }

    public function deletePhoto(): bool {
        if ($this->photo) {
            Storage::delete($this->getPhotoPath().'/'.$this->photo);
            return true;
        }
        return false;
    }

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function questionPackage() {
        return $this->hasOne(QuestionPackage::class, 'id', 'question_package_id');
    }

    public function course() {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class, 'question_id', 'id');
    }
}

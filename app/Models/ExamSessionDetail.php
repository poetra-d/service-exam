<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamSessionDetail extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.exam_session_detail';

    protected $fillable = [
        'organization_id',
        'exam_id',
        'exam_session_id',
        'student_candidate_id',
        'type',
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function exam() {
        return $this->hasOne(Exam::class, 'id', 'exam_id');
    }

    public function examSession() {
        return $this->hasOne(ExamSession::class, 'id', 'exam_session_id');
    }

    public function studentCandidates() {
        return $this->hasMany(StudentCandidate::class, 'student_candidate_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdministratorData extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'org.administrator_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'administrator_id',
        'photo',
        'phone',
        'pob',
        'dob',
        'gender',
        'religion',
        'address',
        'village_name',
        'district_name',
        'city_name',
        'postal_code',
        'nik', 'nuptk', 'division', 'last_education', 'graduation_year', 'majors_courses',
        'university', 'letter_of_appointment_of_employees', 'date_of_entry', 'out_date',
        'bpjs_number', 'bpjs_tk_number'
    ];

    protected $appends = [
        'photo_url'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function administrator() {
        return $this->hasOne(Administrator::class, 'id', 'administrator_id');
    }

    public function getPhotoPath() {
        $code = $this->organization->code;
        return ("public/organization/{$code}/administrator");
    }

    public function getPhotoUrlAttribute() {
        $code = $this->organization->code;
        $photo = $this->photo;
        if ($photo == null) return null;
        return url(Storage::url("organization/$code/administrator/$photo"));
    }

    public function deletePhoto(): bool {
        if ($this->photo) {
            Storage::delete($this->getPhotoPath().'/'.$this->photo);
            return true;
        }
        return false;
    }
}

<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class QuestionAnswer extends BaseModel
{
    const IS_CORRECT = 1;

    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.question_answer';

    protected $fillable = [
        'organization_id',
        'question_id',
        'answer',
        'is_correct',
        'actived'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function question() {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }
}

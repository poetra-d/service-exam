<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Exam extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'exam.exam';

    protected $fillable = [
        'organization_id',
        'course_id',
        'duration',
        'description',
        'actived'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function course() {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function examDetails()
    {
        return $this->hasMany(ExamDetail::class, 'exam', 'id');
    }

    public function questionPackages()
    {
        return $this->hasMany(ExamDetail::class, 'exam', 'id');
    }
}

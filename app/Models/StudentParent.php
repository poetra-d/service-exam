<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class StudentParent extends BaseModel
{
    use HasFactory;

    protected $table = 'org.student_parent';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'student_id',
        'name',
        'email',
        'relation',
        'phone',
        'actived',
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function student() {
        return $this->hasOne(Student::class, 'id', 'student_id');
    }

    public function userClient() {
        return $this->hasOne(UserClient::class, 'id', 'external_id')
            ->where('external_table', UserClient::EXTERNAL_STUDENT_PARENT);
    }
}

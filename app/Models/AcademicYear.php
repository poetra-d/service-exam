<?php

namespace App\Models;

use App\Blameable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AcademicYear extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'lms.academic_year';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'curriculum_id',
        'code',
        'name',
        'start_date',
        'end_date',
        'actived'
    ];

    protected $dates = ['deleted_at'];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function academicSemesters() {
        return $this->hasMany(AcademicSemester::class, 'academic_year_id', 'id')->orderBy('created_at')->orderBy('code');
    }

    public function curriculum() {
        return $this->hasOne(Curriculum::class, 'id', 'curriculum_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class MRole extends BaseModel
{
    use HasFactory;

    const ROLE_CODE_ADMIN = 'ADMIN';
    const ROLE_CODE_TEACHER = 'GURU';
    const ROLE_CODE_STAFF = 'STAFF';
    const ROLE_CODE_HEADMASTER = 'KEPALASEKOLAH';

    const TYPE_ROLE = 'role';
    const TYPE_PERMISSION = 'permission';

    protected $table = 'org.m_role';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'type',
        'actived',
    ];

    protected $hidden = [
        // 'id',
        'created_at',
        'updated_at'
    ];

    public function mRolePermissions()
    {
        return $this->belongsToMany(self::class, 'org.m_role_access', 'm_role_id', 'm_role_permission_id');
    }
}

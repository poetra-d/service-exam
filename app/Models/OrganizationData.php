<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrganizationData extends BaseModel
{
    use HasFactory;

    protected $table = 'org.organization_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'logo',
        'address',
        'email',
        'website',
        'phone',
        'village_name',
        'district_name',
        'city_name',
        'province_name',
        'postal_code',
        'headmaster_name',
        'headmaster_nip',
    ];
}

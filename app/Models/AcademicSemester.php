<?php

namespace App\Models;

use App\Blameable;
use App\Models\AcademicYear;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AcademicSemester extends BaseModel
{
    use HasFactory, SoftDeletes, Blameable;

    protected $table = 'lms.academic_semester';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'academic_year_id',
        'name',
        'code',
        'start_date',
        'end_date',
        'actived'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function academicYear() {
        return $this->hasOne(AcademicYear::class, 'id', 'academic_year_id');
    }
}

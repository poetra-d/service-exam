<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Course extends BaseModel
{
    use HasFactory;

    protected $table = 'lms.course';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'organization_id',
        'code',
        'name',
        'extracurricular'
    ];

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }
}

<?php

namespace App\Models;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MMenu extends BaseModel
{
    use HasFactory;

    protected function getOrgId() {
        $payload = JWTAuth::parseToken()->getPayload();
        return $payload['org'] ?? null;
    }

    protected $table = 'org.m_menu';

    protected $fillable = [
        'parent_id',
        'platform',
        'name',
        'url',
        'order',
        'icon',
        'link_type'
    ];

    public function menuRoleAccess()
    {
        return $this->hasMany(MRoleAccess::class, 'm_role_permission_id');
    }

    public function parent()
    {
        return $this->belongsTo(MMenu::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(MMenu::class, 'parent_id', 'id')
            ->select(['parent_id', 'id', 'name', 'url', 'order', 'link_type', 'icon'])
            ->orderBy('order');
    }

    public function scopeByParentId($query, $parent_id = null)
    {
        return $query->where('parent_id', $parent_id)->with('children')
                        ->select('id', 'name', 'order', 'url', 'link_type', 'icon')
                        ->orderBy('order');
    }

    public function scopeRoleByParentId($query, $parent_id = null, $role)
    {
        $allowedMenus = [];
        if ($role == []) {
            $allowedMenus;
        } elseif (collect($role)->contains('code', MRole::ROLE_CODE_TEACHER) 
                    && collect($role)->contains('code', MRole::ROLE_CODE_STAFF)
                    && collect($role)->contains('code', MRole::ROLE_CODE_ADMIN)) {
            $allowedMenus = $this->pluck('name')->toArray();
        } elseif (collect($role)->contains('code', MRole::ROLE_CODE_TEACHER) 
                    && collect($role)->contains('code', MRole::ROLE_CODE_ADMIN)) {
            $allowedMenus = $this->pluck('name')->toArray();
        } elseif (collect($role)->contains('code', MRole::ROLE_CODE_STAFF) 
                    && collect($role)->contains('code', MRole::ROLE_CODE_ADMIN)) {
            $allowedMenus = $this->pluck('name')->toArray();
        } elseif (collect($role)->contains('code', MRole::ROLE_CODE_TEACHER) 
                    && collect($role)->contains('code', MRole::ROLE_CODE_STAFF)) {
            $allowedMenus = ['Dashboard', 'Kurikulum', 'KBM', 'Rapor', 'Ujian', 'Administrasi'];
        } elseif ($role == MRole::ROLE_CODE_TEACHER) {
            $allowedMenus = ['Dashboard', 'KBM', 'Rapor', 'Ujian', 'Administrasi'];
        } elseif ($role == MRole::ROLE_CODE_STAFF) {
            $allowedMenus = ['Dashboard', 'Kurikulum', 'KBM', 'Rapor', 'Ujian', 'Administrasi'];
        } elseif ($role == MRole::ROLE_CODE_ADMIN) {
            $allowedMenus = $this->pluck('name')->toArray();
        } else {
            $allowedMenus;
        }

        return $query->whereIn('name', $allowedMenus)
            ->where('parent_id', $parent_id)
            ->with('children')
            ->select('id', 'name', 'order', 'url', 'link_type', 'icon')
            ->orderBy('order');
    }

    public function scopeWithMenuRoleAccess($query, $role)
    {
        return $query->select('id', 'name', 'order', 'url', 'link_type', 'icon')
            ->whereIn('id', function($query) use ($role) {
                $query->select('m_role_permission_id')
                    ->from('org.m_role_access')
                    ->where('m_role_id', $role)
                    ->orderBy('order');
            })->with(['children']);
    }

    public function getParentRoot()
    {
        if ($this->parent_id === null) {
            return $this;
        }
        $model = $this;
        parent:
        if ($model !== null) {
            $model = $model->parent;
        }
        if ($model->parent_id !== null) goto parent;
        return $model;
    }

}
